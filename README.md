LURCH
=====

History
-------
Hudson and later Jenkins are two forks of a large scale continous build/integration framework. Written in Java and fairly monolithic I
think they have a few flaws:
* performance is an issue for installation with hundreds of build jobs
* running plugin in the same Java engine means that updating plugins and main Jenkins driver independently is hard
* lack of workspace management means that a job that changes build node (slave) will not get a work space that is consistent with what was on the old build node.
* preferably the system should be able to handle restarts of the main controller without losing job data
* Javas memory management does not sit well with long running services

Goals for lurch
----------------
* get experience with programming a serious project in C++
* create a framework for a robust CI controller
* a constant footprint server
** No memory leaks
** No garbage collection
